require 'spec_helper'
require 'uses_temp_dir'

RSpec.describe PowerStencil::Project::Base do

  include_context :uses_temp_dir

  let(:temp_project_dir) do
    pd = File.join temp_dir, 'project_dir'
    UniverseCompiler::Universe::Base.instance_eval do
      @universes = nil
    end
    described_class.create_project_tree pd
    pd
  end

  let(:temp_project_subdir) do
    ps = File.join temp_project_dir, 'project_subdir'
    FileUtils.mkdir_p ps
    ps
  end

  let(:temp_non_project_empty_dir) do
    sf = File.join temp_dir, 'empty_dir'
    FileUtils.mkdir_p sf
    sf
  end

  subject do
    described_class.new search_from_path: temp_project_dir
  end

  context 'when there is a project tree correctly defined' do

    it 'should be possible to instantiate a project' do
      expect { subject }.not_to raise_error
    end

    context 'when in a subfolder' do

      subject {described_class.new search_from_path: temp_project_subdir}

      it 'should find the project tree' do
        expect(subject.started_from).to eq temp_project_subdir
        expect(subject.project_root).to eq temp_project_dir
      end

    end

  end

  context 'when there is no project tree correctly defined' do

    subject {described_class.new search_from_path: temp_non_project_empty_dir}

    it 'should not be possible to instantiate a project' do
      expect { subject }.to raise_error PowerStencil::Error
    end

  end

end