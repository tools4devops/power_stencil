require 'spec_helper'

RSpec.describe PowerStencil do

  subject {described_class}
  let(:root_command_option_names) { %w(simulate verbose help version project-path debug debug-on-stderr log-level log-file truncate-log-file auto) }

  it 'has a version number' do
    expect(described_class::VERSION).not_to be nil
  end

  it 'should have a logger' do
    expect(subject).to respond_to :logger
  end

  it 'should have a config' do
    expect(subject).to respond_to :config
  end

  it 'should have some basic command line options by default' do
    expect(subject.command_line_manager.root_command.options.map(&:name).to_set).to eq root_command_option_names.to_set
  end
end
