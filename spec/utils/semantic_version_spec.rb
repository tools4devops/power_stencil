require 'spec_helper'

RSpec.describe PowerStencil::Utils::SemanticVersion do

  let(:correct_versions) { ['0.1.3', '0.1.1', '2.3.4-dev', { major: 0, minor: 3, patch: 1, foo: :bar }]}
  let(:incorrect_versions) { ['0.1', '0.1.a', '2.3.4.dev', { minor: 3, patch: 1 }]}

  subject { described_class }

  it 'should be possible to provide nil or empty version' do
    expect { subject.new nil }.not_to raise_error
    expect { subject.new '' }.not_to raise_error
  end

  it 'should be possible to assign valid semantic versions' do
    correct_versions.each do |version|
      expect { subject.new version }.not_to raise_error
    end
  end

  it 'should not be possible to assign invalid semantic versions' do
    incorrect_versions.each do |version|
      expect { subject.new version }.to raise_error PowerStencil::Error
    end
  end

  it 'should be comparable' do
    expect(subject.new '1.1.9').to be < subject.new('1.1.10')
    expect(subject.new '1.1.1').to be > subject.new('1.1.0-xyz')
    expect(subject.new '0.5.1').to be < subject.new('1.1.0-xyz')
    expect(subject.new '1.1.0').to be > subject.new('1.1.0-xyz')
    expect(subject.new '1.1.0-abc').to be < subject.new('1.1.0-xyz')
    expect(subject.new '1.1.0-xy').to be < subject.new('1.1.0-xyz')
    expect(subject.new '1.1.0-xyza').to be > subject.new('1.1.0-xyz')
    expect(subject.new '1.1.0-xyz').to eq subject.new('1.1.0-xyz')
  end

  it 'should be comparable with strings' do
    expect(subject.new '1.1.9').to be < '1.1.10'
    expect(subject.new '1.1.1').to be > '1.1.0-xyz'
    expect(subject.new '0.5.1').to be < '1.1.0-xyz'
    expect(subject.new '1.1.0').to be > '1.1.0-xyz'
    expect(subject.new '1.1.0-abc').to be < '1.1.0-xyz'
    expect(subject.new '1.1.0-xy').to be < '1.1.0-xyz'
    expect(subject.new '1.1.0-xyza').to be > '1.1.0-xyz'
    expect(subject.new '1.1.0-xyz').to eq '1.1.0-xyz'

  end

  it 'should be incrementable' do
    expect(subject.new('1.1.1').succ.to_s).to eq '1.1.2'
    expect(subject.new('1.1.9').succ.to_s).to eq '1.1.10'
    expect(subject.new('1.1.1').succ(:patch)).to eq subject.new('1.1.1').succ
    expect(subject.new('1.1.1').succ(:major).to_s).to eq '2.0.0'
    expect(subject.new('1.1.1').succ(:minor).to_s).to eq '1.2.0'
    expect(subject.new('1.1.1-dev').succ.to_s).to eq '1.1.2-dev'
    expect(subject.new('1.1.1-dev').succ(:major).to_s).to eq '2.0.0-dev'
    expect(subject.new('1.1.1-dev').succ(:minor).to_s).to eq '1.2.0-dev'
  end

  it 'should be incrementable "in place"' do
    expect(subject.new('1.1.1').succ!.to_s).to eq '1.1.2'
    expect(subject.new('1.1.1').succ!(:patch)).to eq subject.new('1.1.1').succ
    expect(subject.new('1.1.1').succ!(:major).to_s).to eq '2.0.0'
    expect(subject.new('1.1.1').succ!(:minor).to_s).to eq '1.2.0'
    expect(subject.new('1.1.1-dev').succ!.to_s).to eq '1.1.2-dev'
    expect(subject.new('1.1.1-dev').succ!(:major).to_s).to eq '2.0.0-dev'
    expect(subject.new('1.1.1-dev').succ!(:minor).to_s).to eq '1.2.0-dev'
  end



end