require 'spec_helper'

RSpec.describe PowerStencil::Utils::DependencySolver do

  let(:dependencies) do
    {
        'plugin1' => %w(plugin2 plugin3),
        'plugin2' => %w(plugin3),
        'plugin3' => [],
        'plugin4' => []
    }
  end

  let(:dependencies_solved) { %w(plugin3 plugin2 plugin1 plugin4) }

  subject { described_class.new.merge dependencies }

  it 'should be possible to get the list of dependencies in the right order' do
    expect(subject.tsort).to eq dependencies_solved
  end

  context 'when dependencies are circular' do
    let(:dependencies) {  { 1 => [2], 2 => [3], 3 => [1] } }

    it 'should fail with a cyclic exception' do
      expect { subject.tsort }.to raise_error TSort::Cyclic
    end


  end

end
