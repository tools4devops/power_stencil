require 'bundler/setup'

$DO_NOT_AUTOSTART_CLIMATIC = true
require 'climatic'
if ENV['RUN_TESTS_IN_DEBUG']
  debug_logger = Logger.new STDERR
  debug_logger.level = Logger::Severity::DEBUG
  Climatic.logger = debug_logger
end

class Climatic::ConfigLayers::ExecutableGemLayer
  def self.executable_gem_config_root
    File.expand_path File.join('..', '..'), __FILE__
  end
end

$GEM_PATH = File.expand_path File.join('..', '..'), __FILE__
$TEST_PATH = File.join $GEM_PATH, 'test'
$TEST_PROJECT_PATH = File.join $TEST_PATH, 'test_project'

require 'power_stencil'
PowerStencil.bootstrap []

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
