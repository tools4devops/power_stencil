require 'spec_helper'
require 'tmpdir'


RSpec.describe PowerStencil::CommandProcessors::Info do

  let(:project_path) { $TEST_PROJECT_PATH }

  subject do
    PowerStencil.config[:'project-path'] = project_path
    PowerStencil.command_line_manager.cmd_line_args = ['info', '--project-path', project_path]
    processors = PowerStencil.command_line_manager.send :processors_hash
    processors['info'].first
  end


  it 'should not raise any exception' do
    expect { subject.execute }.not_to raise_error
  end

  it 'should have a valid project' do
    expect(subject.project).not_to be_nil
  end



end 