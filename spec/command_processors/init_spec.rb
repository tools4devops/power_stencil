require 'spec_helper'
require 'tmpdir'


RSpec.describe PowerStencil::CommandProcessors::Init do
  before(:all) { PowerStencil.config[:'project-path'] = nil }

  # around(:example) do |example|
  #   Dir.mktmpdir 'tests_rspec' do |tmpdir|
  #     PowerStencil.command_line_manager.cmd_line_args = ['init', tmpdir, '--force']
  #     example.run
  #   end
  # end

  around(:example) do |example|
    Dir.mktmpdir do |tmpdir|
      PowerStencil.command_line_manager.cmd_line_args = ['init', tmpdir, '--force']
      example.metadata[:current_tmpdir] = tmpdir
      example.run
    end
  end

  subject do
    processors = PowerStencil.command_line_manager.send :processors_hash
    processors['init'].first
  end

  it 'should create a skeleton of project' do
    expect { subject.execute }.not_to raise_error
    expect(Dir.exists? PowerStencil.command_line_manager.cmd_line_args[1]).to be_truthy
  end


end 
