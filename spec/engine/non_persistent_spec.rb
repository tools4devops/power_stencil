require 'spec_helper'

RSpec.describe PowerStencil::Engine::Base do

  let(:project) {PowerStencil.project}

  subject do
    project.engine
  end

  context 'when invalid files exist in the storage' do

    it 'should load and report invalid files' do
      expect {subject }.not_to raise_error
    end

  end

  it 'should load all valid entities' do
    e = nil
    expect {e = subject.root_universe.get_entity :foo, 'A Foo object'}.not_to raise_error
    expect(e).not_to be_nil
    expect(e[:unexpected_prop]).to eq 'Hey'
    expect(e.fields[:unexpected_prop]).to eq 'Hey'
  end



end