module Sample
  module EntityDefinitions

    class Bar < PowerStencil::SystemEntityDefinitions::ProjectEntity

      include PowerStencil::SystemEntityDefinitions::EntityTemplates
      extend PowerStencil::SystemEntityDefinitions::Buildable

      entity_type :bar

      buildable_by 'sample'

      has_one :foo

      field :file_to_display

    end

  end
end