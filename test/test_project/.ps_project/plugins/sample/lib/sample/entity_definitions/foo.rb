module Sample
  module EntityDefinitions

    class Foo < PowerStencil::SystemEntityDefinitions::ProjectEntity

      entity_type :foo

      field_accessor :stupid

    end

  end
end