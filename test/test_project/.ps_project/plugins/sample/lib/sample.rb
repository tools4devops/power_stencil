require 'sample/sample_processor'
require 'sample/dsl/sample_dsl'


module PowerStencil::Plugin::Sample

  def self.post_build_hook(entity_to_build, files_path)
    unless %i(foo bar).include? entity_to_build.type
      raise PowerStencil::Error, "Sample plugin cannot build '#{entity_to_build.as_path}' !"
    end
    file_to_display = File.expand_path File.join(files_path, entity_to_build.file_to_display)
    puts File.read(file_to_display)
  end
end
