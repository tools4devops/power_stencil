RSpec.describe <%= plugin_module_name %> do

  include PowerStencilTests::Project
  temporary_project 'test_project', scope: :all

  let(:plugin_list) { 'plugin --list' }
  let(:test_entity) { '<%= plugin_name %>_entity/test_<%= plugin_name %>' }
  let(:create_<%= plugin_name %>) { "create #{test_entity}" }
  let(:get_<%= plugin_name %>) { "get #{test_entity}" }

  it 'has a version number' do
    expect(<%= plugin_module_name %>::VERSION).not_to be nil
  end

  it 'should add a "<%= plugin_name %>" sub-command' do
    expect(`#{temporary_project_cmd plugin_list}`).to match '- <%= plugin_name %>'
  end

  it 'should be possible to create a "<%= plugin_name %>_entity"' do
    `#{temporary_project_cmd create_<%= plugin_name %>}`
    expect($?.success?).to be_truthy
    expect(`#{temporary_project_cmd get_<%= plugin_name %>}`).not_to be_empty
  end

  it 'should do something useful' do
    pending 'Tests implementation'
    RSpec.fail
  end

end
