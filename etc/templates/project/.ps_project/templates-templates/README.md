PowerStencil project templates templates
========================================


In this directory, you can define project specific templates for entity types defined locally in this project. 

Templates can be a whole tree structure in a directory named from the entity type of entities you defined in `entity_definitions` folder that are `buildable`.

__It is strongly advised to keep this directory under source control__

This should be the PowerStencil default behaviour. 