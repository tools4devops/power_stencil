PowerStencil entity definitions
===============================


In this directory, you can define project specific entity definitions
 which basically are plain ruby files processed in alphabetical order.
 
You can create classes that are inheriting from `PowerStencil::SystemEntityDefinitions::ProjectEntity`
To express relations between entities you should use the ActiveRecord-like
syntax provided by the `universe_compiler` gem. 


__It is strongly advised to keep this directory under source control__

This should be the PowerStencil default behaviour.

example class:

```ruby
class ExampleEntity < PowerStencil::SystemEntityDefinitions::ProjectEntity
    entity_type :example

    [...]

end
```