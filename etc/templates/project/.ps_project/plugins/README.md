PowerStencil plugins
====================


In this directory, you can define project specific local plugins.
You should create them using the `PowerStencil` command-line.

    $ power_stencil plugin --create myplugin 

__It is strongly advised to keep this directory under source control__

This should be the PowerStencil default behaviour. 