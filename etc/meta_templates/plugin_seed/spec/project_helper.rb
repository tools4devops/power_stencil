module PowerStencilTests

  module Project

    TEMP_DIR_PREFIX = 'PS_TESTS_'

    module ClassMethods


      def temporary_project(project_name, with_git_support: false, scope: :all)
        self.instance_eval do
          puts scope
          around(scope) do |execution|
            Dir.mktmpdir(TEMP_DIR_PREFIX) do |tests_root_dir|
              tmp_project_path = File.join tests_root_dir, project_name
              create_project_including_this_plugin tmp_project_path, with_git_support: with_git_support
              execution.run
              @tmp_project_path = nil
            end
          end
        end
      end

      def plugin_path
        File.expand_path File.join('..', '..'), __FILE__
      end

    end

    attr_reader :tmp_project_path

    def self.included(base)
      base.extend(ClassMethods)
    end

    def create_project_including_this_plugin(project_path, with_git_support: false)
      cmd = "power_stencil init --project-path '#{project_path}'"
      cmd << ' --no-git' unless with_git_support
      `#{cmd}`
      project_plugin_path = File.join project_path, '.ps_project', 'plugins'
      `ln -s '#{self.class.plugin_path}' '#{project_plugin_path}'`
      @tmp_project_path = project_path
    end

    def temporary_project_cmd(params)
      "power_stencil #{params} --project-path '#{tmp_project_path}'"
    end

  end
end