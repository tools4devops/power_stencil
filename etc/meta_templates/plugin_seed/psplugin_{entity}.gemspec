
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require '<%= plugin_name %>/version'

Gem::Specification.new do |spec|
  spec.name          = 'psplugin_<%= plugin_name %>'
  spec.version       = <%= plugin_module_name %>::VERSION
  spec.authors       = ['Your name']
  spec.email         = ['you@somewhere']

  spec.summary       = %q{<%= plugin_name %> is a plugin for the PowerStencil framework.}
  spec.description   = %q{TODO: Write a longer description or delete this line.}
  # For official plugins
  # spec.homepage      = "https://powerstencil.brizone.org/#plugins"
  spec.homepage      = "TODO: Put your gem's website or public repo URL here."
  spec.license       = 'MIT'

  # Change this and following metadata if you don't want your plugin to be an "official" PowerStencil plugin.
  # ie deployed to https://gitlab.com/tools4devops/psplugins
  source_code_uri = 'https://gitlab.com/tools4devops/psplugins/<%= plugin_name %>'

  # Gem metadata
  if spec.respond_to?(:metadata)
    # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
    # to allow pushing to a single host or delete this line to allow pushing to any host.
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

    # Nice link to your home page on rubygems.org
    spec.metadata['homepage_uri'] = spec.homepage

    # You have to probably change this if you don't deploy to gitlab
    spec.metadata['bug_tracker_uri'] = "#{source_code_uri}/issues"
    spec.metadata['documentation_uri'] = "#{source_code_uri}/blob/master/README.md"
    spec.metadata['source_code_uri'] = source_code_uri

    # This metadata is mandatory for a PowerStencil plugin !!
    spec.metadata['plugin_name'] = '<%= plugin_name %>'
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem pushes and PowerStencil plugin mechanism !'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'power_stencil', "~> <%= PowerStencil::VERSION %>"

end
