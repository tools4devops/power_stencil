<% gem_name = "psplugin_#{ plugin_name}" -%>
<%= plugin_title_name %>
<%= '=' * plugin_title_name.size %>

[![Gem Version](https://badge.fury.io/rb/<%= gem_name %>.svg)](https://rubygems.org/gems/<%= gem_name %>) [![Pipeline status](https://gitlab.com/tools4devops/psplugins/<%= plugin_name %>/badges/master/pipeline.svg)](https://gitlab.com/tools4devops/psplugins/<%= gem_name %>/commits/master)

[PowerStencil] is the Swiss-army knife templating workflow for developers and ops.

`<%= plugin_title_name %>` is a [PowerStencil] plugin.

See [official website][PowerStencil site].


<!-- TOC -->

- [What is a PowerStencil plugin ?](#what-is-a-powerstencil-plugin-)
- [Using this plugin in your `PowerStencil` projects](#using-this-plugin-in-your-powerstencil-projects)
- [Goal of this plugin](#goal-of-this-plugin)
- [Plugin dependencies](#plugin-dependencies)
    - [Standard gems dependencies](#standard-gems-dependencies)
    - [Dependency to other plugins](#dependency-to-other-plugins)
- [Plugin capabilities](#plugin-capabilities)
    - [Config](#config)
    - [Subcommands and options](#subcommands-and-options)
    - [Post-build actions](#post-build-actions)
    - [DSL in templates and in `power_stencil shell`](#dsl-in-templates-and-in-power_stencil-shell)
    - [Entity types](#entity-types)
    - [Templates-templates](#templates-templates)
- [Contributing](#contributing)
    - [License](#license)
    - [Code of Conduct](#code-of-conduct)

<!-- /TOC -->

# What is a PowerStencil plugin ?

A `PowerStencil` plugin can be used in the context of a `PowerStencil` project and bring extra features to a standard project.

If you don't know what `PowerStencil` is made for, you may read [the documentation][PowerStencil] first.

Features provided by a plugin can be:

* Extra config.
* Extra subcommands or options added to the `power_stencil` command-line.
* Extra post-build actions.
* Extra DSL methods available in templates and in `power_stencil shell`.
* Extra entity types.
* Extra templates-templates.

# Using this plugin in your `PowerStencil` projects

To use this plugin within your `PowerStencil` project, you need to:

* be within a `PowerStencil` project :wink:.
* declare this plugin in the project configuration (from the root of your project in the `.ps_project/versioned-config.yaml` file).

In this yaml configuration file, you just have to add a new entry in the `:project_plugins` array or simply create it if it is not yet existing:

```yaml
:project_plugins:
  - <%= gem_name %>
```
If this plugin is not already present on your machine, you may have to download it:

    $ power_stencil plugin --install

And then you may see information about the plugin by running:

* `power_stencil info` in the plugins section.
* `power_stencil plugin --list -v`

# Goal of this plugin

# Plugin dependencies

## Standard gems dependencies

Standard dependencies are declared normally in the _gemspec file_. They are automatically installed when installing the plugin with `power_stencil plugin --install`.

## Dependency to other plugins

For development purpose, dependency to other plugins are declared in the _gemspec file_ as _development dependency_. 

But within a project, dependencies have to be added to the `:project_plugins` array in the project configuration file (`.ps_project/versioned-config.yaml`).


# Plugin capabilities

This plugin provides the following features:

## Config

## Subcommands and options

## Post-build actions

## DSL in templates and in `power_stencil shell`

## Entity types

## Templates-templates

# Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/tools4devops/psplugins/<%= plugin_name %>/issues. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License].

## Code of Conduct

Everyone interacting in the PowerStencil project’s codebase, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct].

<!-- End of Document -->

<!-- Pages -->
[code of conduct]: CODE_OF_CONDUCT.md

<!-- Code links -->

<!-- Illustrations -->
[simple-flow-image]: doc/images/power-stencil-simple-flow.svg

<!-- External links -->
[MIT License]: http://opensource.org/licenses/MIT "The MIT license"
[ERB]: https://ruby-doc.org/stdlib-2.6.3/libdoc/erb/rdoc/ERB.html "Quick ERB description"
[Haml]: http://haml.info/ "The templating engine for XML-like documents"
[Ruby]: https://www.ruby-lang.org "The powerful Ruby language"
[Rails]: https://rubyonrails.org/ "The Ruby on Rails framework"
[PowerStencil site]: https://powerstencil.brizone.org "Official PowerStencil website"
[PowerStencil]: https://gitlab.com/tools4devops/power_stencil/blob/master/README.md "PowerStencil documentation"
[PowerStencil plugins]: https://gitlab.com/tools4devops/power_stencil/blob/master/doc/plugins.md "PowerStencil plugins documentation"
