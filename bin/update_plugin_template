#!/usr/bin/env ruby


require 'bundler/setup'
require 'tmpdir'
require 'power_stencil'

LOGGER = Logger.new STDERR

def logger
  LOGGER
end

include Climatic::Proxy
include PowerStencil::Utils::FileHelper
include PowerStencil::Utils::DirectoryProcessor

POWER_STENCIL_ETC = File.expand_path File.join( '..', '..', 'etc'), __FILE__
POWER_STENCIL_TEMPLATES_PATH = File.join POWER_STENCIL_ETC, 'templates'
PLUGIN_DEFINITION_PATH = File.join POWER_STENCIL_TEMPLATES_PATH,'plugin_definition'
PLUGIN_SEED_PATH = File.join POWER_STENCIL_ETC, 'meta_templates', 'plugin_seed'
BACKUP_PATH = "%s-#{timestamp Time.now}.old" % [PLUGIN_DEFINITION_PATH]
# puts "Plugin definition path: '#{PLUGIN_DEFINITION_PATH}'", "Backup path:            '#{BACKUP_PATH}'"

def generate_gem(gem_name, dir)
  Dir.chdir dir do
    # Let's create a new gem using bundle
    res = `bundle gem #{gem_name} --no-exe`
    # Remove useless files from generated gem source
    logger.debug "#{res}"
    gem_path = File.join dir, gem_name
    substituted_path = File.join dir, 'plugin_definition'
    gitdir = File.join gem_path,'.git'
    libdir = File.join gem_path,'lib'
    gemspecfile = File.join gem_path, "#{gem_name}.gemspec"
    FileUtils.remove_entry_secure gitdir
    FileUtils.remove_entry_secure libdir
    FileUtils.remove_entry_secure gemspecfile
    # Copy files from power_stencil seed files
    FileUtils.copy_entry PLUGIN_SEED_PATH, gem_path
    # Perform file names and content substitutions
    process_directory source: gem_path,
                      destination: substituted_path do |source_file, potential_target_file|
      next if File.directory? source_file
      target_file = potential_target_file.gsub gem_name, '{entity}'
      logger.info "Processing #{source_file} -> #{target_file}"
      file_content = File.read source_file
      file_content.gsub! gem_name, '<%= plugin_module_name %>'
      modes = File.stat(source_file).mode
      FileUtils.mkdir_p(File.dirname target_file)
      File.open(target_file, 'w') do |file|
        file.puts file_content
      end
      logger.debug "Processed #{target_file}"
    end

  end
end

def update_definition_template(source_dir, dest_dir)
  # Backup old plugin template definition in power stencil gem
  FileUtils.mv PLUGIN_DEFINITION_PATH, BACKUP_PATH
  # Copy new definition into gem
  FileUtils.copy_entry source_dir, dest_dir
end

fake_gem_name = 'XXXXXXXXXXXX'

Dir.mktmpdir do |tmpdir|
  Dir.chdir tmpdir do
    generate_gem fake_gem_name, tmpdir
    generated_gem_path = File.join(tmpdir, 'plugin_definition')
    update_definition_template generated_gem_path, PLUGIN_DEFINITION_PATH
    puts 'bye'
  end
end



