module PowerStencil
  module Utils

    module Graphviz

      include UniverseCompiler::Utils::Graphviz

      def graph_entities(entities, filename = nil)
        entities_to_process = unless config[:graphviz][:show_config]
                                entities.reject {|e| e.type == :project_config}
                              else
                                entities
                              end

        graph_options = {
            output_file_name: filename,
            graph_type:       config[:graphviz][:graph_type],
            file_type:        config[:graphviz][:file_type]
        }

        graph_entities_to_file entities_to_process, graph_options do |graph, cache|
          graph[:label] = config[:graphviz][:label]
          graph[:labelloc] = config[:graphviz][:labelloc]
          graph.each_node do |node_name|
            node = graph.get_node node_name
            entity = cache[:by_node][node]

            # Set default node attributes
            config[:graphviz][:node].each do |k,v|
              node[k] = v
            end
            fields_to_display = {}

            # Define node tooltip
            entity.fields.keys.each do |field_name|
              constraints = entity.class.fields_constraints[field_name]
              unless constraints.nil?
                # Remove relations as they are represented as edges
                next if constraints.keys.include? :has_one
                next if constraints.keys.include? :has_many
              end
              fields_to_display[field_name] = entity[field_name]
            end
            node[:tooltip] = fields_to_display.to_yaml

            # specific customizations
            if config[:graphviz][:customizations][entity.type]
              config[:graphviz][:customizations][entity.type].each do |k,v|
                node[k] = v
              end
            end
          end
        end

      end


    end

  end
end