module PowerStencil
  module Utils

    module Completion

      class FakeIgnoreList
        attr_accessor :return_value
        def ignore_file?(file)
          return_value
        end
      end

      include PowerStencil::Utils::DirectoryProcessor

      def generate_zsh_completion(script_name, target_file, generating_project_completion)

        source_dir = File.join PowerStencil::Project::Paths.system_templates_templates_path, 'zsh_command_line_completion'
        source_template = File.join source_dir, '_power_stencil.sh.erb'

        engine = PowerStencil::Engine::InitEngine.new
        engine.dsl = PowerStencil::Dsl::Completion
        engine.dsl.script_name = script_name
        engine.dsl.generating_project_completion = generating_project_completion

        engine.instance_eval do
          @files_not_to_rename = FakeIgnoreList.new
          files_not_to_rename.return_value = true
          @files_not_to_render = FakeIgnoreList.new
          files_not_to_render.return_value = false
        end

        engine.render_single_template_file source_template, target_file

      end

    end

  end
end