module PowerStencil
  module Utils

    module EntityHelper

      EntitySearchReference = Struct.new(:type, :name) do

        def self.from_string(string)
          if md = string.match(/^\s*(?<type>[^\/]+)\/\s*(?<name>.+)\s*$/)
            return new md[:type].to_sym, md[:name]
          end

          false
        end

        def as_path
          to_a.map(&:to_s).join '/'
        end

        def to_a
          [type, name]
        end

        def fill(something)
          something.type = type
          something.name = name
        end

      end


    end

  end
end