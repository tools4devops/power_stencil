module PowerStencil
  module Utils

    module GemUtils

      def plugin_definition_to_name_and_req(plugin_definition)
        md = plugin_definition.match /^\s*([^\s]+)\s*,?\s*(.+)?\s*$/
        if md.nil?
          raise PowerStencil::Error, "Invalid plugin_definition '#{plugin_definition}'"
        end
        gem_name = md[1]
        gem_req = if md[2].nil? or md[2].empty?
                    ::Gem::Requirement.default
                  else
                    ::Gem::Requirement.create md[2]
                  end
        [gem_name, gem_req]
      end

      def find_gemspec_manually(gem_name)
        gem_spec_files = []
        ::Gem::Specification.dirs.each do |dir|
          Dir.glob(File.join(dir,  "#{gem_name}-*.*.*.gemspec")) do |file|
            gem_spec_files << file
          end
        end
        gem_spec_files.map { |file| ::Gem::Specification.load file }
      end

    end

  end
end
