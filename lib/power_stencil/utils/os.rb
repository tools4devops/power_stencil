module PowerStencil
  module Utils

    module Os

      WINDOWS_RUBY_FLAVOURS = %w(mswin32 cygwin mingw bccwin)

      def self.windows?
        WINDOWS_RUBY_FLAVOURS.include? RbConfig::CONFIG['host_os']
      end

    end

  end
end
