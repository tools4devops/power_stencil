module Slop

  # :file, :path, :log_level, :string, :array, :entity_type, :scenario

  class StringOption
    ZSH_TYPE = :power_stencil_do_nothing
  end

  class ArrayOption
    ZSH_TYPE = :power_stencil_do_nothing
  end

  class BuildableOption
    ZSH_TYPE = :power_stencil_buildable
  end

  class LogLevelOption < IntegerOption
    ZSH_TYPE = :power_stencil_log_level
  end

  class PathOption < StringOption
    ZSH_TYPE = :path_files
  end
  class FileOption < StringOption
    ZSH_TYPE = :files
  end

  class EntityOption < StringOption
    ZSH_TYPE = :power_stencil_entity
  end

  class EntityTypeOption < StringOption
    ZSH_TYPE = :power_stencil_entity_type
  end

  class ScenarioOption < StringOption
    ZSH_TYPE = :power_stencil_scenario
  end

  class CompletionQueryTypeOption < StringOption
    ZSH_TYPE = :power_stencil_completion_query_type
  end

end