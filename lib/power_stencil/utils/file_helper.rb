module PowerStencil
  module Utils

    module FileHelper

      def find_recursively_in_path(dir_or_file, from_dir)
        raise PowerStencil::Error, "Invalid directory '#{from_dir}'" unless Dir.exist?(from_dir) and File.readable?(from_dir)

        candidate = File.join from_dir, dir_or_file

        if Dir.exists? candidate or File.exist? candidate
          if File.readable? candidate and File.writable? candidate
            candidate
          else
            PowerStencil.logger.warn "Humm, found something: '#{candidate}', but with wrong access rights."
            nil
          end
        else
          next_dir = File.expand_path '..', from_dir
          if next_dir == from_dir
            PowerStencil.logger.info "Didn't find any trace of '#{dir_or_file}'... :("
            nil
          else
            find_recursively_in_path dir_or_file, next_dir
          end
        end

      end

      def yaml_file_to_hash(file_name)
        raw_content = File.read file_name
        res = YAML.load raw_content
        if res
          Hash[res.map { |k, v| [k, v] }]
        else
          raise PowerStencil::Error, "Invalid file content for '#{file_name}'" unless raw_content.empty?
          []
        end
      end

      def timestamped_uniq_dir(seed, start_time)
        uniq_dir = timestamp start_time
        uniq_dir += "-#{seed}" unless seed.nil?
        uniq_dir
      end

      def timestamp(timedate)
        timedate.strftime '%Y%m%d-%H%M-%S%L'
      end



    end

  end
end