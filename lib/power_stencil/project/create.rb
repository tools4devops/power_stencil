require 'fileutils'

module PowerStencil
  module Project

    module Create

      INITIAL_REPOSITORY_COMMIT_MESSAGE = 'Initial commit for project "%s".'

      include Climatic::Proxy

      def create_project_tree(path, config_directory_name = PowerStencil.config[:default_config_directory_name])
        path = File.expand_path path
        config_path = File.join path, config_directory_name
        if Dir.exists? config_path
          raise PowerStencil::Error, "The directory '#{path}' already contains a PowerStencil project !" unless config[:force]
        end
        logger.info "Creating project in '#{path}'..."
        render_project_template_in path
        initialize_git_repository path
      end

      private

      def initialize_git_repository(repo_path)
        if config[:'no-git']
          puts_and_logs 'Do not initialize project git repository as per config request.', logs_as: :debug
          return
        end
        if Dir.exists? File.join(repo_path, '.git')
          puts_and_logs 'Git repository already exists. Skipping.', logs_as: :debug, check_verbose: false
          return
        end
        puts_and_logs 'Initializing git repository...', logs_as: :debug
        git = ::Git.init repo_path
        logger.debug 'Adding all files.'
        git.add repo_path
        logger.debug 'Committing initial status'
        commit_msg = INITIAL_REPOSITORY_COMMIT_MESSAGE % [File.basename(repo_path)]
        git.commit_all commit_msg
      end

      def render_project_template_in(new_project_config_path)
        engine = PowerStencil::Engine::InitEngine.new
        engine.render_source PowerStencil::Project::Paths.project_system_template_template_path, new_project_config_path
      end

    end

  end
end
