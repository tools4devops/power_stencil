require 'power_stencil/utils/dependency_solver'

require 'power_stencil/project/paths'
require 'power_stencil/project/create'
require 'power_stencil/project/config'
require 'power_stencil/project/versioning'
require 'power_stencil/project/info'
require 'power_stencil/project/templates'
require 'power_stencil/project/plugins'
require 'power_stencil/project/git'
require 'power_stencil/project/completion'
require 'power_stencil/project/require_entity_definitions'

require 'power_stencil/engine/project_engine'
require 'power_stencil/engine/entity_engine'

module PowerStencil
  module Project

    class Base

      class << self

        def instantiate_from_config(config)
          path = config[:'project-path'] || Dir.pwd
          new search_from_path: path
        end

      end

      include Climatic::Proxy
      include PowerStencil::Project::Paths
      include PowerStencil::Project::Config
      include PowerStencil::Project::Versioning
      include PowerStencil::Project::Templates
      include PowerStencil::Project::Plugins
      include PowerStencil::Project::Info
      include PowerStencil::Project::Git
      include PowerStencil::Project::Completion
      extend PowerStencil::Project::Create

      attr_reader :engine, :entity_engine

      def name
        File.dirname project_config_root
      end

      def initialize(search_from_path: Dir.pwd)
        initialize_paths search_from_path
        load_project_specific_config
        check_project_version
        bootstrap_plugins
        build_engines
        register_system_templates_templates
        register_plugins_templates_templates
        register_project_templates_templates
        setup_git_tracking
      end

      private

      def register_plugins_templates_templates
        plugins.each do |_, plugin|
          plugin.register_plugin_templates_templates
        end
      end


      def register_project_templates_templates
        dir = project_templates_templates_path
        if Dir.exist? dir and File.readable? dir
          logger.info 'Registering project specific templates.'
          Dir.entries(dir).each do |potential_entity_type|
            next if potential_entity_type.match /^\./
            template_dir = File.join(dir, potential_entity_type)
            next unless File.directory? template_dir
            register_template_template_path_for_type potential_entity_type.to_sym, template_dir
          end
        end
      end

      def register_system_templates_templates
        logger.debug 'Registering system templates'
        %i(plugin_definition simple_exec).each do |template_name|
          template_path = system_template_template_path template_name
          register_template_template_path_for_type template_name, template_path
        end
      end


      def build_engines
        @engine = PowerStencil::Engine::ProjectEngine.new self
        @entity_engine = PowerStencil::Engine::EntityEngine.new
      end

    end

  end
end
