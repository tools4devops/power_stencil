module PowerStencil
  module Project

    module RequireEntityDefinition

      def securely_require_with_entity_class_detection(source, &block)
        before = PowerStencil::Engine::EntitiesHandling.all_types
        yield
        after = PowerStencil::Engine::EntitiesHandling.all_types
        after.reject { |k, _| before.keys.include? k }.each do |_, defined_entity_class|
          provider = source
          defined_entity_class.instance_eval do
            @entity_type_source_provider = provider
          end
        end
      end

    end

  end
end
