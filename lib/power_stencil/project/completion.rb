module PowerStencil
  module Project

    module Completion

      def query_for_completion(query_type)
        case query_type
        when :entities
          engine.entities.map(&:as_path).sort
        when :'entity-types'
          engine.available_entity_types.sort
        when :scenario
          engine.entities(criterion: :by_type, value: :entity_override) do |entity|
            !entity.scenario.nil? and !entity.scenario.empty?
          end.map(&:scenario).sort.uniq
        when :buildable
          engine.entities(&:buildable?).map(&:as_path).sort
        else
          raise PowerStencil::Error, "'#{query_type}' is not a valid query type for completion !"
        end
      end

    end

  end
end

