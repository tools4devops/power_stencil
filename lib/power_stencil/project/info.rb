module PowerStencil
  module Project

    module Info

      def paths_report
        report = []
        report << "Project root path: '#{project_root}'"
        report << "Project configuration path: '#{project_config_root}'"
        report << if File.exist? project_personal_config_file
                    "Local user configuration file: '#{project_personal_config_file}'"
                  else
                    'No personal project config file found'
                  end
        report << if File.exist? project_versioned_config_file
                    "Project (versioned) configuration file: '#{project_versioned_config_file}'"
                  else
                    'No versioned project config file found'
                  end
        report << "Project entities: '#{project_entities_path}'"
        report << "User (unversioned) entities: '#{user_entities_path}'"
        txt = "Project specific entity definitions: '#{project_entity_definitions_path}'"
        txt << ' (missing)' unless Dir.exist? project_entity_definitions_path
        report << txt
      end

      def general_report
        report = []
        report << "Project required version: #{config[:min_power_stencil_version]}" unless config[:min_power_stencil_version].nil?
        report << "PowerStencil version: #{PowerStencil::VERSION}"
      end

      def plugin_report(plugin_name, plugin)
        report = []
        # report << "Plugin '#{plugin_name}'"
        plugin.capabilities.each {|name, status| report << "#{name.to_s}: #{status}"}
        report
      end

      def entities_report
        report = []
        used_types = {}
        report << "Contains #{engine.root_universe.get_entities.count} entities."
        engine.root_universe.get_entities
            .sort{ |a,b| a.type <=> b.type }
            .each do |entity|
          used_types[entity.type] ||= 0
          used_types[entity.type] += 1
        end
        used_types.each do |type, count|
          report << " #{type}: #{count}"
        end
        report
      end

      def entity_types_report
        report = []
        PowerStencil::Engine::EntitiesHandling.all_types
            .select { |type, _| type.is_a? Symbol}
            .sort{ |a,b| a.first <=> b.first }
            .each do |type, klass|
          msg = "Type '#{type}' --> #{klass}"

          source_provider = klass.entity_type_source_provider
          source_provider_display = if source_provider == PowerStencil
                                      "'#{source_provider.name}'"
                                    elsif source_provider.is_a? self.class
                                      "project '#{source_provider.name}'"
                                    elsif source_provider.is_a? PowerStencil::Plugins::Base
                                      "plugin '#{source_provider.name}'"
                                    else
                                      raise PowerStencil::Error, "Unidentified source provider for #{klass} !"
                                    end

          msg << " (provided by #{source_provider_display})"
          msg << " template-template path: '#{entity_type_templates_templates[type]}'" unless entity_type_templates_templates[type].nil?
          report << msg
        end
        report
      end

    end

  end
end
