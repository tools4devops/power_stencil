module PowerStencil
  module Project

    module Versioning

      def check_project_version
        return if project_version_valid?
        msg = <<~EOM
            This PowerStencil project requires a version of PowerStencil >= #{config[:min_power_stencil_version]} (You are using version #{PowerStencil::VERSION}) !
            You should consider upgrading by doing 'gem install power_stencil'...
        EOM
        raise PowerStencil::Error, msg.chomp
      end

      private

      def project_version_valid?
        unless config[:min_power_stencil_version].nil?
          framework_version = PowerStencil::Utils::SemanticVersion.new PowerStencil::VERSION
          is_valid = framework_version >= config[:min_power_stencil_version]
          if is_valid
            logger.debug "PowerStencil version (v: #{framework_version}) is advanced enough to handle project required version (v: #{config[:min_power_stencil_version]})"
          end
          return is_valid
        end
        logger.debug 'Project does not require any specific PowerStencil version'
        true
      end

    end

  end
end
