module PowerStencil
  module Project

    module Git

      include Climatic::Utils::Input

      def track_action_with_git(action_message,
                                user_validation_required: false,
                                validation_message: 'Commit changes ?',
                                show_files_to_commit: false,
                                &block)
        original_path = Dir.pwd
        return yield if git.nil?

        Dir.chdir project_root
        status_before_action = git.status
        yield
        status_after_action = git.status

        files_introduced_by_action = status_after_action.untracked.reject { |f| status_before_action.untracked.keys.include? f}
        files_deleted_by_action = status_after_action.deleted.reject { |f| status_before_action.deleted.keys.include? f}
        files_modified_by_action = status_after_action.changed.reject { |f| status_before_action.changed.keys.include? f}
        files_to_commit = [files_introduced_by_action, files_deleted_by_action, files_modified_by_action].map(&:keys).flatten.sort.uniq

        if files_to_commit.empty?
          puts_and_logs 'Nothing to commit.'
          return
        end

        if show_files_to_commit
          header = 'Following file'
          header << 's' if files_to_commit.count > 1
          header << ' will be committed:'
          puts header
          puts files_to_commit.map { |filename| " - '#{filename}'" }
        end

        if user_validation_required
          unless get_user_confirmation(default_choice: 'Yes', prompt: validation_message)
            puts_and_logs 'Commit cancelled by user.', check_verbose: false
            return
          end
        end

        files_to_commit.each { |filename| git.add filename }

        # Verify files to be committed really are
        files_really_to_be_committed = git.diff.stats[:files].keys.select { |filename| files_to_commit.include? filename }
        return if files_really_to_be_committed.empty?
        files_really_to_be_committed.each { |filename| logger.info "File '#{filename}' will be committed" }

        git.commit action_message
        puts_and_logs 'All changes introduced have been committed.'
      ensure
        Dir.chdir original_path
      end

      private

      attr_reader :git

      def setup_git_tracking
        require 'git'
        @git = nil
        if config[:'no-git']
          logger.debug "Won't track any changes with git as per config request!"
          return
        end
        @git = ::Git.open(self.project_root, :log => PowerStencil.logger)
        logger.debug 'Following project changes with git'
      rescue => e
        logger.debug PowerStencil::Error.report_error(e)
        logger.warn 'This project is not managed by git'
        # Ensure Git will not be further used
        @git = nil
      end

    end

  end
end
