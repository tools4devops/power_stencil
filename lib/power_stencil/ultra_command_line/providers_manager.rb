module PowerStencil
  module UltraCommandLine

    module ProvidersManager

      def providers_manager
        PowerStencil.command_line_manager.class
      end

      def providers
        providers_manager.providers_for self
      end

      def add_provider(provider)
        providers_manager.add_provider_for self, provider
      end

    end

  end
end