class UltraCommandLine::Commands::SubCommand

  include PowerStencil::UltraCommandLine::ProvidersManager

  def add_provider(provider)
    super
    options.each do |option|
      option.add_provider provider
    end
  end

end