class UltraCommandLine::Manager::Base

  class << self

    def add_provider_for(command_or_option, provider)
      providers = providers_for command_or_option
      providers << provider unless providers.include? provider
    end

    def providers_for(command_or_option)
      default_command_data = {options: {}, providers: []}
      case command_or_option
      when UltraCommandLine::Commands::SubCommand
        sc = command_or_option
        internal_provider_mapping[sc.name] ||= default_command_data
        internal_provider_mapping[sc.name][:providers]
      when UltraCommandLine::Commands::OptionDefinition
        sco = command_or_option
        internal_provider_mapping[sco.sub_command.name] ||= default_command_data
        internal_provider_mapping[sco.sub_command.name][:options][sco.name] ||= []
        internal_provider_mapping[sco.sub_command.name][:options][sco.name]
      else
        raise PowerStencil::Error, "Invalid command or option"
      end
    end


    def internal_provider_mapping
      @internal_provider_mapping ||= {}
    end

  end

  # def contribute_to_definition(hash, layer_name: :unknown_layer)
  #   super
  # end



  def processor
    processor_candidates = processors_hash.fetch(command(cmd_line_args).name, []) .select do |processor|
      processor.check_params command.cmd_line_args
    end
    raise UltraCommandLine::Error, 'No processor found for this command line' if processor_candidates.empty?
    return processor_candidates.first if processor_candidates.size == 1


    raise UltraCommandLine::Error, 'Too many possible processors' unless processor_candidates.size == 1
    processor_candidates.first
  end

end