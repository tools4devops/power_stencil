module PowerStencil
  module CommandProcessors

    class Create

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper
      include PowerStencil::Utils::FileEdit

      def execute
        analyse_extra_params.each do |search_criterion|
          begin
            puts_and_logs "Creating new entity '#{search_criterion.as_path}'...", check_verbose: false
            entity_as_hash = {name: search_criterion.name}
            unless config[:property].nil?
              config[:property].each do |property_def|
                if md = property_def.match(/^\s*(?<prop_name>[^:=]+)\s*[:=]\s*(?<prop_val>.*)\s*$/)
                  property_value = md['prop_val']
                  logger.debug "Using extra properties from command line '#{md['prop_name']}' = '#{property_value}'"
                  if pd = property_value.match(/^\s*!entity\s+(?<entity_id>.+)\s*$/)
                    property_value = pd['entity_id']
                    esr = PowerStencil::Utils::EntityHelper::EntitySearchReference.from_string property_value
                    if esr
                      type, name = esr.to_a
                      referenced_entity = project.engine.entity(type, name)
                      property_value = referenced_entity.to_reference unless referenced_entity.nil?
                      puts_and_logs "Could not find entity '#{type.to_s}/#{name}' !", logs_as: :error, check_verbose: false if referenced_entity.nil?
                    end
                  end
                  entity_as_hash[md['prop_name'].to_sym] = property_value


                else
                  raise PowerStencil::Error, "Invalid command line property definition: '#{property_def}'"
                end
              end
            end

            new_entity = project.engine.new_entity project.engine.root_universe,
                                                   search_criterion.type,
                                                   fields: entity_as_hash,
                                                   user: config[:user]

            if config[:edit]
              logger.debug "Edit mode for entity #{new_entity.to_composite_key} in #{new_entity.source_uri}"
              new_entity = edit_before_save new_entity
            end

            new_entity.resolve_fields_references!

            new_entity.valid? raise_error: true
            project.track_action_with_git("Created '#{new_entity.as_path}' (and potential dependencies).") do
              new_entity.save
            end
            puts_and_logs "Generated templates in '#{new_entity.templates_path}'.", check_verbose: false if new_entity.buildable?
          rescue => e
            logger.debug PowerStencil::Error.report_error(e)
            raise PowerStencil::Error, "Failed to create '#{search_criterion.as_path}' with message '#{e.message}'."
          end
        end
      end

      private

      def modifications_valid?(modified_path, original_entity)
        test_entity = UniverseCompiler::Entity::Persistence.load modified_path
        test_universe = project.engine.root_universe.dup
        duplicated_entity = test_universe.get_entity *(original_entity.to_composite_key)
        test_universe.replace duplicated_entity, test_entity
        test_entity.resolve_fields_references!
        test_entity.valid? raise_error: true
      end

      def edit_before_save(entity)
        modified_entity = securely_edit_entity entity do |modified_path, _|
          modifications_valid? modified_path, entity
        end
        project.engine.root_universe.replace entity, modified_entity
        modified_entity
      end


    end

  end
end