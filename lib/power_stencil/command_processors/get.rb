module PowerStencil
  module CommandProcessors

    class Get

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper
      include PowerStencil::Utils::Graphviz

      def execute
        targets = targets_from_criteria analyse_extra_params, project.engine.root_universe
        if config[:graph] || config[:'graph-file']
          filename = graph_entities targets, config[:'graph-file']
          msg = "Graphviz graph saved into '#{filename}'."
          logger.info msg
          puts msg
          if config[:graphviz][:viewer]
            cmd = config[:graphviz][:viewer] % [filename]
            `#{cmd}`
          end
        else
          targets.sort{|a, b| a.as_path <=> b.as_path}.each {|target| display_entity target }
        end
      end
    end

  end

end