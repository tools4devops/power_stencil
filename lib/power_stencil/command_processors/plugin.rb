module PowerStencil
  module CommandProcessors

    class Plugin

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy

      def execute

        if config[:install]
          begin
            project
          rescue
            # Do not check errors. This is just to load project config...
            logger.debug "Project config loaded"
          end
          if config[:project_plugins].empty?
            puts_and_logs "No plugins declared... Check your config file.", check_verbose: false
            return
          end
          config[:project_plugins].each do |plugin_definition|
            gem_name, gem_req = PowerStencil::Plugins::Base.plugin_definition_to_name_and_req plugin_definition
            logger.debug "Trying to install plugin '#{gem_name}'..."
            specs = PowerStencil::Plugins::Base.install_gem gem_name, gem_req
            spec = specs.first
            puts_and_logs "Installed plugin '#{spec.name}' (version: #{spec.version})", check_verbose: false
          end
          return
        end

        if config[:list]
          if project.plugins.empty?
            puts 'No plugin used in this project.'
          else
            puts "#{project.plugins.size} plugin#{project.plugins.size == 1 ? '' : 's'} found to be used in this project."
          end
          project.plugins.each do |_, plugin|
            puts " - #{plugin.name} (in '#{plugin.plugin_path}')"
            if config[:verbose]
              plugin.capabilities.each do |name, value|
                puts "   #{name}: #{value}"
              end
            end
          end
          return
        end

        if config[:create]
          plugin_name = config[:create]
          begin
            target_path = File.join project.project_local_plugin_path(plugin_name)
            msg = "Generated new local plugin '#{plugin_name}'."
            project.track_action_with_git(msg) do
              project.create_new_local_plugin_tree plugin_name, target_path
              puts_and_logs msg, check_verbose: false
            end
          rescue => e
            msg = "Could not create plugin '#{plugin_name}' because '#{e.message}'"
            puts msg
            logger.error "Could not create plugin '#{plugin_name}' because '#{e.message}'"
            logger.debug PowerStencil::Error.report_error(e)
          end

          return
        end

        raise PowerStencil::Error, 'You should specify an option'

      end

    end

  end
end