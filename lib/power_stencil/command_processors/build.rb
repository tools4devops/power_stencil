module PowerStencil
  module CommandProcessors

    class Build

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper



      def execute

        if config[:'supported-builds']
          project.engine.available_entities_hash.select do |_, klass|
            klass.respond_to?(:buildable?) and klass.buildable?
          end .each do |entity_type, entity_class|
            builder = entity_class.buildable_by.empty? ? "'#{PowerStencil.name}'" : "plugin '#{entity_class.buildable_by}'"

            source_provider = entity_class.entity_type_source_provider
            source_provider_display = if source_provider == PowerStencil
                                        "'#{source_provider.name}'"
                                      elsif source_provider.is_a? project.class
                                        "project '#{source_provider.name}'"
                                      elsif source_provider.is_a? PowerStencil::Plugins::Base
                                        "plugin '#{source_provider.name}'"
                                      else
                                        raise PowerStencil::Error, "Unidentified source provider for #{entity_class} !"
                                      end

            puts " - '#{entity_type}' (buildable by #{builder} and provided by #{source_provider_display})."
          end
          return
        end

        if config.command_line_layer.extra_parameters.empty?
          raise PowerStencil::Error, 'Please specify something to build'
        end

        if config[:'build-tag']
          raise PowerStencil::Error, "Invalid tag name '#{config[:'build-tag']}'" if config[:'build-tag'].include? '/'
          raise PowerStencil::Error, "Cannot tag a build under Windows" if PowerStencil::Utils::Os.windows?
        end

        targets = targets_from_criteria analyse_extra_params, project.engine.root_universe
        raise PowerStencil::Error, 'No valid entity specified' if targets.empty?
        project.engine.build targets, fail_on_error: config[:'fail-on-error'], parallelized: config[:parallelized]
        build_ok = true
        project.engine.last_build_report.each do |entity_full_name, entity_report|
          if entity_report == ['Ok']
            puts " - '#{entity_full_name}' has been correctly built"
          else
            build_ok = false
            puts_and_logs " - Failed to build '#{entity_full_name}' because #{entity_report.join}", logs_as: :error, check_verbose: false
          end
        end
        unless build_ok
          raise PowerStencil::Error, 'Could not complete build !'
        end

      end

    end

  end
end