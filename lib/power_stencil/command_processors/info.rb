module PowerStencil
  module CommandProcessors

    class Info

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy

      def execute
        puts '-' * 80
        puts 'PROJECT REPORT'
        puts '-' * 80
        puts ' General information:'
        puts(project.general_report.map { |p| ' - %s' % [p] })
        puts '-' * 80
        puts ' Paths:'
        puts(project.paths_report.map { |p| ' - %s' % [p] })
        unless project.plugins.empty?
          puts '-' * 80
          puts ' Plugins:'
          project.plugins.each do |plugin_name, plugin|
            puts " --> Plugin '#{plugin_name}' has following capabilities:"
            puts(project.plugin_report(plugin_name, plugin).map { |p| '   - %s' % [p] })
          end
        end
        puts '-' * 80
        puts ' Entities:'
        puts(project.entities_report.map { |p| ' - %s' % [p] })
        puts '-' * 80
        puts ' Available entity types:'
        puts(project.entity_types_report.map { |p| ' - %s' % [p] })
      end

    end

  end
end