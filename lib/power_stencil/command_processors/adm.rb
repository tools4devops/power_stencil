module PowerStencil
  module CommandProcessors

    class Adm

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::Utils::Completion


      def execute

        if config[:'zsh-completion']
          target_dir = File.expand_path config[:completion_target][:zsh][:completion_dir]
          script_name = 'power_stencil'
          user_completion_script = File.join target_dir, "_#{script_name}"
          begin
            current_dir = Dir.pwd
            Dir.mktmpdir 'completion_generation' do |tmpdir|
              Dir.chdir tmpdir
              generate_zsh_completion script_name, user_completion_script, false
            end
          ensure
            Dir.chdir current_dir
          end

          begin
            project
            shortname_project_completion_script = File.join config[:default_config_directory_name], config[:completion_target][:zsh][:project_completion_filename]
            project.track_action_with_git("Adding project specific zsh completion file: '#{ shortname_project_completion_script }'.") do
              project_completion_script = File.join project.project_config_root, config[:completion_target][:zsh][:project_completion_filename]
              generate_zsh_completion script_name, project_completion_script, true
              puts_and_logs "A specific completion has been generated for this project in '#{project_completion_script}'.", check_verbose: false
            end
          rescue
            # Do not check errors. This is just to load project config...
            logger.debug "Outside of a PowerStencil project... Not generating zsh project completion."
          end
          puts_and_logs "zsh global auto_completion has been installed in '#{user_completion_script}'.", check_verbose: false
          puts
          puts "You should ensure you have something like 'fpath=(#{target_dir} $fpath)' in your ~/.zshrc file..."
          puts 'You may have to relog for changes to be applied.'
          return
        end

        if config[:'query-for-completion']
          puts project.query_for_completion config[:'query-for-completion'].to_sym
        end

      end

    end

  end
end

