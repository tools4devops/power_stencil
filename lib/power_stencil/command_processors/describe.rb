module PowerStencil
  module CommandProcessors

    class Describe

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper

      def execute
        targets = extra_params_to_entity_types
        describe_entity_types targets
      end
    end

  end

end