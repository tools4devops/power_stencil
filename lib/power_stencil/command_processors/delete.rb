module PowerStencil
  module CommandProcessors

    class Delete

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper

      def execute
        targets = targets_from_criteria analyse_extra_params, project.engine.root_universe
        targets.each do |target|
          begin
            # unless project.engine.entity *target.to_a, project.engine.root_universe
            #   puts_and_logs "Skipping '#{target.as_path}'. Entity not found.", check_verbose: false
            #   next
            # end
            puts_and_logs "Deleting entity '#{target.as_path}'", check_verbose: false

            msg = "Deleted entity '#{target.as_path}' (and potential dependencies)"
            msg << ' including templates.' if config[:'delete-files']
            msg << '.'
            project.track_action_with_git(msg) do
              if project.engine.delete_entity project.engine.root_universe,
                                              target.type,
                                              target.name,
                                              delete_files: config[:'delete-files']
                msg = "Deleted '#{target.as_path}'"
                msg << ' and template files.' if config[:'delete-files']
                puts_and_logs msg, check_verbose: false
              else
                puts_and_logs 'Cancelled by user input.', check_verbose: false
              end
            end
          rescue => e
            puts_and_logs "Failed to delete '#{target.as_path}' with message '#{e.message}'.", logs_as: :error, check_verbose: false
            logger.debug PowerStencil::Error.report_error(e)
          end
        end
      end

    end

  end
end