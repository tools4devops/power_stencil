module PowerStencil
  module CommandProcessors

    class Edit

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper
      include PowerStencil::Utils::FileEdit

      def execute
        targets = targets_from_criteria analyse_extra_params, project.engine.root_universe
        targets.each do |target|
          project.track_action_with_git("Edited entity '#{target.as_path}'.") do
            securely_edit_file target.source_uri do |modified_path, _|
              modifications_valid? modified_path, target
            end
          end
        end
      end

      private

      def modifications_valid?(modified_path, original_entity)
        test_entity = UniverseCompiler::Entity::Persistence.load modified_path
        test_universe = project.engine.root_universe.dup
        duplicated_entity = test_universe.get_entity *(original_entity.to_composite_key)
        test_universe.replace duplicated_entity, test_entity
        test_entity.resolve_fields_references!
        test_entity.valid? raise_error: true
      end

    end

  end
end