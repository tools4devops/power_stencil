module PowerStencil
  module CommandProcessors

    class Check

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper

      def execute
        targets = targets_from_criteria analyse_extra_params, project.engine.root_universe
        sorted_targets = targets.sort {|a,b| a.as_path <=> b.as_path }
        if config[:'invalid-only']
          sorted_targets.reject! &:valid?
          if sorted_targets.empty?
            puts 'No invalid entity found in this repository.'
            return
          else
            puts 'Showing invalid entities only:'
          end
        end
        sorted_targets.map do |entity|
          entity_info = [" '#{entity.as_path}':"]
          entity_info << "  - Storage path   : '#{entity.source_uri}'"

          source_provider = entity.class.entity_type_source_provider
          source_provider_display = if source_provider == PowerStencil
                                      "'#{source_provider.name}'"
                                    elsif source_provider.is_a? project.class
                                      "project '#{source_provider.name}'"
                                    elsif source_provider.is_a? PowerStencil::Plugins::Base
                                      "plugin '#{source_provider.name}'"
                                    else
                                      raise PowerStencil::Error, "Unidentified source provider for #{entity.class} !"
                                    end
          entity_info << "  - Provided by    : #{source_provider_display}"
          entity_info << "  - Templates path : '#{entity.templates_path}'" if entity.respond_to? :templates_path
          entity_info << "  - Status         : #{entity.valid? ? 'Valid' : 'INVALID !'} "
          entity_info << "  - Buildable      : #{entity.buildable?}"
          entity_info
        end .each { |entity_info| puts entity_info }
      end


    end

  end
end