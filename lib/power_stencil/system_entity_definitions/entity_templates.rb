module PowerStencil
  module SystemEntityDefinitions

    module EntityTemplates

      def templates_path
        PowerStencil.project.entity_template_path self
      end

    end

  end
end