module PowerStencil
  module SystemEntityDefinitions

    class Plugin < PowerStencil::SystemEntityDefinitions::ProjectEntity

      include PowerStencil::SystemEntityDefinitions::NonPersistent

      DOC = 'A system class not to be used'

      entity_type :plugin_definition

    end


  end
end

