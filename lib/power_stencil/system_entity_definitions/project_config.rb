module PowerStencil
  module SystemEntityDefinitions

    class ProjectConfig < PowerStencil::SystemEntityDefinitions::ProjectEntity

      include PowerStencil::SystemEntityDefinitions::NonPersistent

      DOC = 'A system singleton read-only entity holding runtime PowerStencil configuration'

      entity_type :project_config

      field_reader :simulate, :debug

      def initialize(fields: {}, universe: nil, user: false)
        super
        self.name = 'Project Config'
      end

    end


  end
end

