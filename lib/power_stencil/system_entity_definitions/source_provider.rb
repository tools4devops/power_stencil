module PowerStencil
  module SystemEntityDefinitions

    module SourceProvider

      DEFAULT_PROVIDER = PowerStencil

      def entity_type_source_provider
        @entity_type_source_provider ||= DEFAULT_PROVIDER
      end

    end

  end
end