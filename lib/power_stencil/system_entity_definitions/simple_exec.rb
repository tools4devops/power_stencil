module PowerStencil
  module SystemEntityDefinitions

    class SimpleExec < PowerStencil::SystemEntityDefinitions::ProjectEntity

      include PowerStencil::SystemEntityDefinitions::EntityTemplates

      DOC = 'Describes a simple process to be called after source files have been rendered'.freeze

      entity_type :simple_exec

      buildable

      has_one :process_descriptor, name: :post_process
      not_null :post_process

      def valid?(raise_error: false)
        unless super(raise_error: false)
          if self.post_process.nil?
            self.post_process = PowerStencil.project.engine.new_entity universe, :process_descriptor, fields: {
                name: "simple_exec_#{name}.process",
                process: './main.sh'
            }, user: is_user_entity?
          end
        end
        super(raise_error: raise_error)
      end

      def save(uri = source_uri, raise_error: true, force_save: false, force_files_generation: false )
        valid? raise_error: raise_error
        self.post_process.save
        super
      end

      def delete(force_files_deletion: false)
        super
        if self.post_process.name == "simple_exec_#{name}.process"
          self.post_process.delete
        end
        self
      end


    end

  end
end
