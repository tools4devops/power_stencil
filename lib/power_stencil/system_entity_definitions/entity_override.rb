class UniverseCompiler::Entity::Override

  extend PowerStencil::SystemEntityDefinitions::SourceProvider
  extend PowerStencil::SystemEntityDefinitions::Buildable
  include PowerStencil::SystemEntityDefinitions::EntityProjectCommon

  field :description

  def initialize(fields: {}, universe: nil, user: false)
    @is_versioned_entity = not(user)
    super(fields: fields, universe: universe)
  end

end
