module PowerStencil
  module SystemEntityDefinitions

    module NonPersistent

      def save
        PowerStencil.logger.debug "Won't save '#{self.class.to_s}' because defined as non persistent"
        self
      end

    end

  end
end