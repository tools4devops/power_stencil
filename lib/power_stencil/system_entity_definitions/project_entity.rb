module PowerStencil
  module SystemEntityDefinitions

    class ProjectEntity < UniverseCompiler::Entity::Base

      extend PowerStencil::SystemEntityDefinitions::SourceProvider
      extend PowerStencil::SystemEntityDefinitions::Buildable
      include PowerStencil::SystemEntityDefinitions::EntityProjectCommon

      entity_type :base_entity

      field :description

      def initialize(fields: {}, universe: nil, user: false)
        @is_versioned_entity = not(user)
        super(fields: fields, universe: universe)
      end


    end

  end
end
