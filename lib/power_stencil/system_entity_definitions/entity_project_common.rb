module PowerStencil
  module SystemEntityDefinitions

    module EntityProjectCommon

      def is_versioned_entity?
        @is_versioned_entity
      end

      def is_user_entity?
        not(@is_versioned_entity)
      end

      def save(uri = source_uri, raise_error: true, force_save: false, force_files_generation: false )
        super(source_uri, raise_error: raise_error, force_save: force_save)
        unless PowerStencil.project.entity_type_templates_templates[type].nil?
          PowerStencil.project.generate_template_dir_for_entity self, force: force_files_generation
        end
        self
      end

      def delete(force_files_deletion: false)
        unless PowerStencil.project.entity_type_templates_templates[type].nil?
          PowerStencil.project.delete_template_dir_for_entity self, force: force_files_deletion
        end
        super()
        self
      end

      def buildable_by
        self.class.buildable_by
      end

      def buildable?
        self.class.buildable?
      end

      protected

      def project_engine
        PowerStencil.project.engine
      end

      def create_universe_entity(type, fields = {})
        project_engine.new_entity project_engine.root_universe, type, fields: fields
      end

      def remove_entity_from_universe(entity)
        project_engine.root_universe.delete entity
      end

    end

  end
end
