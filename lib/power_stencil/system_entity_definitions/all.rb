# This file is required by the engine but not from the gem
# So it is safe to declare files in the order you need.

require 'power_stencil/system_entity_definitions/non_persistent'
require 'power_stencil/system_entity_definitions/entity_templates'
require 'power_stencil/system_entity_definitions/source_provider'
require 'power_stencil/system_entity_definitions/buildable'
require 'power_stencil/system_entity_definitions/entity_project_common'

require 'power_stencil/system_entity_definitions/project_entity'
require 'power_stencil/system_entity_definitions/project_config'
require 'power_stencil/system_entity_definitions/plugin'
require 'power_stencil/system_entity_definitions/process_descriptor'
require 'power_stencil/system_entity_definitions/simple_exec'
require 'power_stencil/system_entity_definitions/entity_override'
