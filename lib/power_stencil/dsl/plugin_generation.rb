module PowerStencil
  module Dsl

    class PluginGeneration < PowerStencil::Dsl::Base

      def plugin_name
         main_entry_point.underscore
      end

      def plugin_title_name
        plugin_name.tr('_', ' ').split.map(&:capitalize).join(' ')
      end

      def plugin_module_name
        plugin_name.camelize
      end

      def erb_code(ruby_code_str, skip_line = true)
        end_char = skip_line ? '-' : ''
        '%s %s %s%s' % ['<%', ruby_code_str, end_char, '%>']
      end

      def erb_insert(ruby_code_str)
        '%s %s %s' % ['<%=', ruby_code_str, '%>']
      end

    end

  end
end
