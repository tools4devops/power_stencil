module PowerStencil
  module Dsl

    class Completion < PowerStencil::Dsl::Base

      class << self
        attr_accessor :script_name
        attr_accessor :generating_project_completion
      end

      attr_reader :encountered_types

      def timestamp
        if generating_user_completion?
          " on the #{Time.now}"
        else
          ''
        end
      end

      def initialize(universe)
        super
        @encountered_types = {}
      end

      def generating_project_completion?
        self.class.generating_project_completion
      end

      def generating_user_completion?
        !self.class.generating_project_completion
      end

      def context_commands
        if generating_user_completion?
          PowerStencil.command_line_manager.commands.select do |command|
            command.providers.include? PowerStencil
          end
        else
          PowerStencil.command_line_manager.commands
        end
      end

      def context_options(command)
        if generating_user_completion?
          command.options.select do |option|
            option.providers.include? PowerStencil
          end
        else
          command.options
        end
      end

      def script_name
        self.class.script_name
      end

      def root_command(commands)
        commands.select { |c| c.name.empty? }.first
      end

      def command_summary(command)
        text = command.help[0][0,60].tr("\n", ' ')
        text.match(/^\s*(?<sentence>[^\s][^\.]+)\./) do |md|
          return "#{md['sentence']} ..."
        end
        text
      end

      def command_representation(command, enclosing_dquote: true)
        cr = "#{command.name}[#{command_summary command}]"
        enclosing_dquote ? "\"#{cr}\"" : cr
      end

      def default_command_param_type(command, enclosing_dquote: true)
        return '' if %i(bool boolean).include? command.type
        default_cpt = "*:#{command.type}:_#{script_name}_#{command.type} "
        type = command.type

        encountered_types[type] ||=[]
        encountered_types[type] << command unless encountered_types[type].include? command
        enclosing_dquote ? "\"#{default_cpt}\"" : default_cpt
      end

      def option_representation(option, enclosing_dquote: true)
        opr = "--#{option.name}[#{option.summary}]"
        opr = if option.type.nil? or option.type.empty?
                opr
              else
                if %i(bool boolean).include? option.type
                  opr
                else
                  encountered_types[option.type] ||=[]
                  encountered_types[option.type] << option unless encountered_types[option.type].include? option
                  "#{opr}:#{option.type}:_#{ zsh_completion_from_type option.type} "
                end
              end
        enclosing_dquote ? "\"#{opr}\"" : opr
      end

      def continued_multilines(lines, number_spaces: 4, cr: "\n", spaces_in_first_line: true, str_cont: ' \\')
        return '' if lines.empty?
        spaces = ' ' * number_spaces
        first_line = true
        res = lines.map do |line|
          if first_line
            fline = if spaces_in_first_line
                      "#{spaces}#{line}"
                    else
                      line
                    end
            first_line = false
            fline
          else
            "#{spaces}#{line}"
          end
        end
        eol = str_cont + cr
        res.join eol
      end

      def zsh_completion_from_type(type)
        slop_class = type.to_s.split('_').map(&:capitalize).join + 'Option'
        klass = Slop.const_get slop_class
        klass::ZSH_TYPE
      end

    end

  end
end
