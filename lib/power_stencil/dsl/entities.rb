module PowerStencil
  module Dsl

    module Entities

      def available_entity_types(force_rescan: false)
        PowerStencil.project.engine.available_entity_types force_rescan: force_rescan
      end

      def build_target
        type, name = main_entry_point.split('/')
        e = entity type, name
        e.nil? ? entity(type.to_sym, name) : e
      end

      def project_config
        entity :project_config, 'Project Config'
      end

      def entity(type_or_path, name = nil)
        type, name = if name.nil?
                       esr = PowerStencil::Utils::EntityHelper::EntitySearchReference.from_string type_or_path
                       if esr
                         esr.to_a
                       else
                         raise PowerStencil::Error, "Invalid entity id: '#{type_or_path}'"
                       end
                     else
                       [type_or_path, name]
                     end
        PowerStencil.project.engine.entity type, name, @universe
      end

      def entities(criterion: nil, value: nil, &filter_block)
        PowerStencil.project.engine.entities @universe, criterion: criterion, value: value, &filter_block
      end

      def delete_entity(entity, delete_files: false)
        PowerStencil.project.engine.delete_entity @universe, entity.type, entity.name, delete_files: delete_files
      end

      def method_missing(method_name, *arguments, &block)
        if method_name.to_s =~ /^user_new_(.*)/
          PowerStencil.project.engine.new_entity @universe, $1, fields: Hash[*arguments], user: true, &block
        elsif method_name.to_s =~ /^new_(.*)$/
          PowerStencil.project.engine.new_entity @universe, $1, fields: Hash[*arguments], &block
        else
          PowerStencil.logger.warn "Invalid DSL method: '#{method_name}'"
          super
        end
      end

      def respond_to_missing?(method_name, include_private = false)
        method_name.to_s.start_with?('new_') || super
      end

    end

  end
end
