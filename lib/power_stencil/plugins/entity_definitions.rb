require 'power_stencil/project/require_entity_definitions'

module PowerStencil
  module Plugins

    module EntityDefinitions

      def require_plugin_entity_definitions
        return unless capabilities[:entity_definitions]
        logger.info "Requiring '#{self.name}' plugin entity definitions..."
        securely_require_with_entity_class_detection(self) do
          plugin_definition[:entity_definitions].each do |entity_definition_file_path|
            securely_require entity_definition_file_path, fail_on_error: true
          end
        end

      end

      private

      include PowerStencil::Project::RequireEntityDefinition

    end

  end
end
