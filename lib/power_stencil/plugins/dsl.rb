module PowerStencil
  module Plugins

    module Dsl

      def dsl_modules_names
        return [] if plugin_definition[:dsl].nil?
        case plugin_definition[:dsl]
        when String
          [plugin_definition[:dsl]]
        when Array
          plugin_definition[:dsl]
        else
          raise PowerStencil::Error, "Invalid DSL definition for plugin '#{self.name}' !"
        end
      end

      def dsl_modules
        dsl_modules_names.map { |dsl_module_name| Object.const_get dsl_module_name }
      end

      def apply_extra_dsl(dsl_base)
        dsl_modules.each do |dsl_module|
          logger.debug "Applying extra DSL '#{dsl_module.name}' to base DSL..."
          dsl_base.extend dsl_module
        end

      end



    end

  end
end

