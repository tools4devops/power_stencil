module PowerStencil
  module Plugins

    module Config

      private

      def load_plugin_specific_config
        yaml_file = plugin_config_specific_file
        if File.exists? yaml_file and File.file? yaml_file and File.readable? yaml_file
          logger.info "Found plugin specific config in plugin '#{self.name}'. Attempting to load..."
          project.add_plugin_config self
          capabilities[:config] = true
        end
      rescue => e
        logger.debug PowerStencil::Error.report_error(e)
        logger.error "Could not load yaml file '#{yaml_file}' because '#{e.message}'"
        raise e
      end

    end

  end
end
