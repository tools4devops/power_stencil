module PowerStencil
  module Plugins

    module Capabilities

      include PowerStencil::Utils::FileHelper

      CAPABILITIES = %i(config command_line processors generate_build_files post_build dsl entity_definitions templates).freeze

      attr_reader :plugin_definition

      def capabilities
        @capabilities ||= CAPABILITIES.dup.zip([false] * CAPABILITIES.size).to_h
      end

      def dependencies
        plugin_definition[:dependencies]
      end

      private

      def determine_capabilities
        yaml_file = plugin_capabilities_definition_file
        unless File.exists? yaml_file and File.file? yaml_file and File.readable? yaml_file
          raise PowerStencil::Error, "Plugin '#{self.name}' has no definition file !"
        end
        logger.debug "Loading plugin '#{self.name}' capabilities..."
        begin
          @plugin_definition = yaml_file_to_hash yaml_file
          %i(processors entity_definitions templates generate_build_files post_build dsl).each do |capability|
            unless plugin_definition[capability].nil? or plugin_definition[capability].empty?
              capabilities[capability] = true
            end
          end
        rescue => e
          logger.debug PowerStencil::Error.report_error(e)
          raise PowerStencil::Error, "Invalid plugin capabilities file '#{yaml_file}' for plugin '#{self.name}'"
        end

      end

    end

  end
end