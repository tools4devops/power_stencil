module PowerStencil
  module Plugins

    module Paths

      def plugin_path
        case self.type
        when :local
          project.project_local_plugin_path self.name
        when :gem
          gem_spec.gem_dir
        end
      end

      def plugin_command_line_definition_file
        File.join plugin_path, 'etc', 'command_line.yaml'
      end

      def plugin_capabilities_definition_file
        File.join plugin_path, 'etc', 'plugin_capabilities.yaml'
      end

      def plugin_config_specific_file
        File.join plugin_path, 'etc', 'plugin_config.yaml'
      end

      def plugin_processors_dir
        File.join plugin_path, 'lib', plugin_name, 'processors'
      end

      def plugin_entities_definitions_dir
        File.join plugin_path, 'etc',  plugin_name, 'entities_definitions'
      end

    end

  end
end