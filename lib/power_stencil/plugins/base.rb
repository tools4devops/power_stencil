require 'power_stencil/plugins/paths'
require 'power_stencil/plugins/type'
require 'power_stencil/plugins/config'
require 'power_stencil/plugins/command_line'
require 'power_stencil/plugins/entity_definitions'
require 'power_stencil/plugins/templates'
require 'power_stencil/plugins/require'
require 'power_stencil/plugins/capabilities'
require 'power_stencil/plugins/gem'
require 'power_stencil/plugins/build'
require 'power_stencil/plugins/dsl'

module PowerStencil

  module Plugins

    class Base

      extend PowerStencil::Plugins::Gem

      include Climatic::Proxy
      include PowerStencil::Plugins::Type
      include PowerStencil::Plugins::Paths
      include PowerStencil::Plugins::Config
      include PowerStencil::Plugins::CommandLine
      include PowerStencil::Plugins::Require
      include PowerStencil::Plugins::Capabilities
      include PowerStencil::Plugins::EntityDefinitions
      include PowerStencil::Plugins::Templates
      include PowerStencil::Plugins::Build
      include PowerStencil::Plugins::Dsl

      attr_reader :name, :version, :entry_point_path, :gem_spec

      def initialize(name, project, type: :local, gem_req: nil)
        @name = name
        @project = project
        @version = PowerStencil::Utils::SemanticVersion.new '0.0.0-not-specified'
        raise PowerStencil::Error, "Invalid plugin type (#{type}) for plugin '#{name}'" unless PLUGIN_TYPES.include? type
        @type = type
        case type
        when :gem
          logger.debug "Plugin '#{name}' is provided as a Gem."
          @gem_spec = PowerStencil::Plugins::Base.find_locally_installed_gem_spec name, gem_req
          raise PowerStencil::Error, "Cannot find plugin '#{name}'. Try 'power_stencil plugin --install'" if gem_spec.nil?
          raise PowerStencil::Error, "Invalid plugin '#{name}' ! Missing metadata 'plugin_name' in spec !" if gem_spec.metadata['plugin_name'].nil?
          logger.debug "Plugin '#{name}' real name is '#{gem_spec.metadata['plugin_name']}'."
          @name = gem_spec.metadata['plugin_name']
        when :local
          logger.debug "Plugin '#{name}' is provided locally by the project."
        else
          raise PowerStencil::Error, "Unsupported plugin type (#{type}) for plugin '#{name}' !"
        end

        logger.debug "Loading plugin '#{name}'..."
        setup_plugin
        logger.info "Plugin '#{name}' successfully available"
        logger.debug "Plugin '#{name}' has following capabilities: #{capabilities.inspect}"
      end

      def plugin_module
        Object.const_get plugin_definition[:plugin_module]
      end

      private

      attr_reader :project

      def setup_plugin
        determine_capabilities
        load_plugin_specific_config
        load_yaml_command_definition
      end

    end

  end
end