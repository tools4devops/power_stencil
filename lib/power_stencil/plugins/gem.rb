require 'rubygems/commands/install_command'

module PowerStencil
  module Plugins

    module Gem

      include PowerStencil::Utils::GemUtils

      NO_DOC = %w(--no-document).freeze


      def is_available_gem?(gem_name)

      end

      def install_gem(plugin_name, plugin_requirements)
        ::Gem.install plugin_name, plugin_requirements
      end



      def find_locally_installed_gem_spec(gem_name, gem_requirements = ::Gem::Requirement.default)
        # Normal method to find gem_spec doesn't work in the context of bundler !!
        # candidates = ::Gem::Specification.find_all_by_name gem_name
        candidates = find_gemspec_manually(gem_name)
                         .select {|candidate| gem_requirements.satisfied_by? candidate.version}
                         .sort {|a, b| a.version <=> b.version}
        PowerStencil.logger.error "Could not find required plugin '#{gem_name}'" if candidates.empty?
        candidates.last
      end

    end

  end
end
