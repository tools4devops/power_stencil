module PowerStencil
  module Plugins

    module Require

      POST_BUILD_HOOK = :post_build_hook

      include PowerStencil::Utils::SecureRequire

      def require_entry_point
        @entry_point_path = File.join plugin_path, 'lib', "#{name.underscore}.rb"
        logger.debug "Plugin '#{name}' entry point: '#{entry_point_path}'"
        plugin_root_path = File.dirname(entry_point_path)
        begin
          $LOAD_PATH << plugin_root_path
          securely_require entry_point_path unless plugin_definition[:plugin_module].nil?
        rescue LoadError => e
          @entry_point_path = nil
          logger.warn "As plugin '#{name}' code is invalid, removing '#{plugin_root_path}' from LOAD_PATH"
          $LOAD_PATH.delete plugin_root_path
        end
      end

      private

      def module_short_name
        name.split(/[-_]+/).map(&:capitalize).join.to_sym
      end

      def setup_version
        @version = PowerStencil::Utils::SemanticVersion.new plugin_module::VERSION
        capabilities[:version] = true
        logger.debug "Plugin '#{name}' is in version: #{version}"
      rescue
        logger.warn "No version specified for plugin '#{name}'."
      end

    end

  end
end
