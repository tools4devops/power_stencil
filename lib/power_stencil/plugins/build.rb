module PowerStencil
  module Plugins

    module Build

      def generate_build_files(*args)
        build_method_name = plugin_definition[:generate_build_files]
        msg = "Plugin '#{self.name}' doesn't contain the '#{build_method_name}' method in the '#{plugin_definition[:plugin_module]}' module. Cannot delegate !"
        raise PowerStencil::Error, msg unless plugin_module.respond_to? build_method_name
        plugin_module.send build_method_name, *args
      end

      def post_build_hook(*args)
        build_method_name = plugin_definition[:post_build]
        msg = "Plugin '#{self.name}' doesn't contain the '#{build_method_name}' method in the '#{plugin_definition[:plugin_module]}' module. Cannot delegate !"
        raise PowerStencil::Error, msg unless plugin_module.respond_to? build_method_name
        plugin_module.send build_method_name, *args
      end

    end

  end
end

