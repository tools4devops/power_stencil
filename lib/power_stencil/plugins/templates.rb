module PowerStencil
  module Plugins

    module Templates

      def register_plugin_templates_templates
        return unless capabilities[:templates]
        logger.info "Loading '#{self.name}' plugin templates..."
        plugin_definition[:templates].each do |templates_path|
          plugin_templates_path = File.join self.plugin_path, templates_path
          Dir.entries(plugin_templates_path).reject { |e| %w(. ..).include? e }.each do |entry|
            template_path = File.join(plugin_templates_path, entry)
            if Dir.exist? template_path
              project.register_template_template_path_for_type entry.to_sym, template_path
            end
          end
        end
      end

    end

  end
end
