module PowerStencil
  class Error < StandardError

    DEFAULT_EXIT_CODE = 127

    def exit_code=(val)
      raise PowerStencil::Error, 'Invalid exit code !' unless val.is_a? Fixnum
      @exit_code = val
    end

    def exit_code
      @exit_code ||= DEFAULT_EXIT_CODE
    end

    def self.report_error(e)
      "#{e.message}\nBacktrace:\n#{e.backtrace.join("\n\t")}"
    end

  end
end