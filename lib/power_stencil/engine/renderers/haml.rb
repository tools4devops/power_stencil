module PowerStencil
  module Engine
    module Renderers

      module Haml

        private

        def render_haml_template(source, context)
          logger.debug "Using HAML to render file '#{source}'"
          ::Haml::Engine.new(File.read source).render(context)
        rescue => e
          logger.debug PowerStencil::Error.report_error(e)
          raise PowerStencil::Error, "Error rendering '#{source}': '#{e.message}'"
        end

      end

    end
  end
end
