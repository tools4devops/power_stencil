require 'power_stencil/project/require_entity_definitions'

module PowerStencil
  module Engine

    module EntitiesDefinition

      SYSTEM_ENTITY_DEFINITION_ENTRY_POINT = 'power_stencil/system_entity_definitions/all'.freeze

      include PowerStencil::Utils::SecureRequire

      def require_definition_files(files_or_dirs, source)
        required_files = []
        files_or_dirs.each do |file_or_dir|
          if File.directory? file_or_dir and File.readable? file_or_dir
            Dir.entries(file_or_dir).grep(/\.rb$/).each do |file|
              required_files << File.join(file_or_dir, file)
            end
            next
          end
          if File.file? file_or_dir and File.readable? file_or_dir
            required_files << file_or_dir
            next
          end
          # This is a ruby library or there is something wrong

          # securely_require_with_entity_class_detection file_or_dir, source
          securely_require_with_entity_class_detection(source) do
            securely_require file_or_dir, fail_on_error: true
          end

        end
        required_files.sort!.each do |file|
          securely_require_with_entity_class_detection source do
            securely_require file, fail_on_error: true
          end
        end
        required_files
      end

      private

      include PowerStencil::Project::RequireEntityDefinition

      def load_system_entities_definition
        require_definition_files [SYSTEM_ENTITY_DEFINITION_ENTRY_POINT], PowerStencil
      end

    #   def securely_require_with_entity_class_detection(entity_definition_file_path, source)
    #     before = PowerStencil::Engine::EntitiesHandling.all_types
    #     securely_require entity_definition_file_path, fail_on_error: true
    #     after = PowerStencil::Engine::EntitiesHandling.all_types
    #     after.reject { |k, _| before.keys.include? k }
    #         .each do |_, plugin_entity_class|
    #       plugin_entity_class.instance_eval do
    #         @entity_type_source_provider = source
    #       end
    #     end
    #   end

    end

  end
end
