require 'power_stencil/engine/base'
require 'power_stencil/engine/entities_handling'
require 'power_stencil/engine/build_handling'

module PowerStencil
  module Engine

    class ProjectEngine < PowerStencil::Engine::Base

      include PowerStencil::Engine::EntitiesHandling
      include PowerStencil::Engine::BuildHandling

      attr_reader :project

      def initialize(project)
        super()
        @project = project
        load_system_entities_definition
        load_plugins_entities_definition
        load_project_entities_definition
        load_entities
      end

      def running_context(universe = root_universe, main_entry_point: nil)
        context = dsl.new universe
        apply_plugins_dsl_definition context
        context.main_entry_point = main_entry_point
        context.instance_eval do
          binding
        end
      end
      protected

      def load_project_entities_definition
        dir = project.project_entity_definitions_path
        if Dir.exist? dir and File.readable? dir
          logger.info 'Loading project specific entity definitions.'
          $LOAD_PATH << dir
          require_definition_files [dir], project
        end
      end

      def load_entities
        root_universe.clear
        load_system_entities
        load_project_entities
        load_user_entities
      end

      private

      def load_plugins_entities_definition
        project.plugins_sorted_by_dependency.each do |plugin|
          plugin.require_plugin_entity_definitions
        end
      end

      def apply_plugins_dsl_definition(context)
        project.plugins_sorted_by_dependency.each do |plugin|
          plugin.apply_extra_dsl context
        end
      end

      def load_project_entities
        root_universe.import project.project_entities_path, stop_on_error: false do |new_entity|
          logger.debug "Loaded entity: '#{new_entity.as_path}'"
        end
      end

      def load_user_entities
        root_universe.import project.user_entities_path, stop_on_error: false do |new_entity|
          logger.debug "Loaded unversioned entity: '#{new_entity.as_path}'"
          new_entity.instance_eval do
            @is_versioned_entity = false
          end
        end
      end

    end

  end
end
