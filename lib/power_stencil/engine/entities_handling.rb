module PowerStencil
  module Engine

    module EntitiesHandling

      include Climatic::Utils::Input

      def self.all_types
        entities_base_type = UniverseCompiler::Entity::Base

        klasses = ObjectSpace.each_object(Class).select do |klass|
          klass < entities_base_type and not klass.singleton_class?
        end
        types = klasses.map &:entity_type

        types.zip(klasses).to_h.reject do |t, k|
          t.to_s.include? '/'
        end
      end

      def available_entities_hash(force_rescan: false)
        if force_rescan
          available_entity_types force_rescan: true
        end
        @available_entities_hash
      end

      def available_entity_types(force_rescan: false)
        if force_rescan
          @available_entities_hash = PowerStencil::Engine::EntitiesHandling.all_types
        else
          @available_entities_hash ||= PowerStencil::Engine::EntitiesHandling.all_types
        end
        @available_entities_hash.keys
      end

      def entity(type, name, universe = self.root_universe)
        universe.get_entity type, name
      end

      def entities(universe = self.root_universe, criterion: nil, value: nil, &filter_block)
        universe.get_entities criterion: criterion, value: value, &filter_block
      end

      def delete_entity(work_universe, type, name, delete_files: false)
        entity_to_delete = work_universe.get_entity type, name
        get_user_confirmation prompt: "Are you sure you want delete '#{[type, name].join '/'}' ?" do
          work_universe.delete(entity_to_delete).each do |impacted_entity, _|
            impacted_entity.save raise_error: false, force_save: true
          end
          entity_to_delete.delete force_files_deletion: delete_files
          entity_to_delete
        end
      end

      def new_entity(work_universe, type, fields: {}, user: false)
        logger.debug "Trying to create a new entity in '#{work_universe.name}'"
        unless available_entity_types.include? type.to_sym
          raise PowerStencil::Error, "Unknown entity type: '#{type}'"
        end
        fields[:name] = fields[:name].to_s
        res = @available_entities_hash[type.to_sym].new fields: fields, universe: work_universe, user: user
        logger.debug "Created new '#{type}': \n#{fields.to_yaml}"
        work_universe.add res
        if work_universe == PowerStencil.project.engine.root_universe
          res.source_uri = user ? PowerStencil.project.user_entity_path(res) : PowerStencil.project.project_entity_path(res)
        else
          PowerStencil.logger.warn "Creating entity, but you won't be able to save it in compiled mode !"
        end
        res
      end



    end

  end
end